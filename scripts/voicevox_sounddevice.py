#!/usr/bin/env python3
## coding: UTF-8
# Copyright (c) 2022 Hiroyuki Okada
# All rights reserved.
import rospy
import os, sys
from std_msgs.msg import String
from dynamic_reconfigure.server import Server
from voicevox_ros.cfg import VoiceVoxConfig

from audio_common_msgs.msg import AudioData
import simpleaudio as sa
from io import BytesIO
import sounddevice as sd
import soundfile as sf
import numpy as np

import ctypes
#for CUDA
#ctypes.cdll.LoadLibrary('/voicevox/onnxruntime-linux-x64-gpu-1.13.1/lib/libonnxruntime.so')
#for CPU
ctypes.cdll.LoadLibrary('/voicevox/onnxruntime-linux-x64-1.13.1/lib/libonnxruntime.so')

from pathlib import Path
from voicevox_core import VoicevoxCore, METAS

class voicevox:
    def __init__(self):  #-- 初期値を与える
        # 辞書の読み込み
        self.core = VoicevoxCore(open_jtalk_dict_dir=Path("/voicevox/open_jtalk_dic_utf_8-1.11"))

        # ノードを初期化する。
        rospy.loginfo("start voicevox node")
        rospy.init_node("voicevox")
        # 受信者を作成する。
        rospy.Subscriber("/voicevox/request", String, self.callback)
        #　動的に設定を変更する
       # dyn_srv = Server(VoiceVoxConfig, self.config_callback)

        # を使う
  
        
        # モデルの読み込み
        self.speaker_id=rospy.get_param("/voicevox/speaker_id")
        self.core.load_model(self.speaker_id)  # 指定したidのモデルを読み込む

        # オープンニングのメッセージ
        rospy.loginfo("say opening Message")
        self.opening_msg=rospy.get_param("/voicevox/opening_msg")
        text = self.opening_msg
#        text = "こんにちは、ボイスボックスを使った音声合成ノードを起動します"
        wave_bytes = self.core.tts(text, self.speaker_id)  # 音声合成を行う
        wav_stream = BytesIO(wave_bytes)
        audio_array, sampling_rate = sf.read(wav_stream)
        sd.play(audio_array, sampling_rate)
        sd.wait() # sd.playが完了するのを待つ

        # ノードが終了するまで待機する。
        rospy.spin()
    

    def config_callback(self, config, level):
        rospy.loginfo("""Reconfigure Request: {speaker_id}, {f0_correct}""".format(**config))
        self.speaker_id=config.speaker_id
        return config


    def callback(self, msg):
        rospy.loginfo("Message '{}' recieved".format(msg.data))
        text = msg.data
        self.speaker_id=rospy.get_param("/voicevox/speaker_id")

        # モデルが読み込まれていない場合
        if not self.core.is_model_loaded(self.speaker_id):
            rospy.loginfo("Model_loaded :speaker_id='{}'".format(self.speaker_id))
            self.core.load_model(self.speaker_id)  # 指定したidのモデルを読み込む

        rospy.loginfo("speaker_id : %d", self.speaker_id)
        wave_bytes = self.core.tts(text, self.speaker_id)  # 音声合成を行う

        # 音声の再生
        wav_stream = BytesIO(wave_bytes)
        audio_array, sampling_rate = sf.read(wav_stream)
        sd.play(audio_array, sampling_rate)
        sd.wait() # sd.playが完了するのを待つ


if __name__ == "__main__":
    voicevox()


