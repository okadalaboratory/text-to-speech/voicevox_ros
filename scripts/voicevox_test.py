#!/usr/bin/env python3
## coding: UTF-8
# Copyright (c) 2022 Hiroyuki Okada
# All rights reserved.
import rospy
import os, sys
from std_msgs.msg import String

def speaker():
    pub = rospy.Publisher('voicevox/request', String, queue_size=10)
    rospy.init_node('speaker', anonymous=True)
    r = rospy.Rate(1) # 1hz
    str = "皆さんこんにちは"
    rospy.loginfo(str)
    pub.publish(str)
    r.sleep()

if __name__ == '__main__':
    try:
        speaker()
    except rospy.ROSInterruptException: pass
