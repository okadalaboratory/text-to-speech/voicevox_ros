# VoiceVoxをROS1 Noeticで使う
[VOICEVOX | 無料のテキスト読み上げソフトウェア](https://voicevox.hiroshiba.jp/)

- 商用・非商用問わず無料 <BR>
- すぐに使えるソフトウェア <BR>
Windows, Mac, Ubuntu, Python API
- イントネーションの詳細な調整が可能<br>

ここではROS1 NoeticからVoiceVoxを使ってみます。

[ROS2でVoiceVoxを使う場合はこちら](https://github.com/HansRobo/speak_ros_voicevox_plugin)


![VoiceVox ROS](https://gitlab.com/okadalaboratory/audio/text-to-speech/voicevox_ros/-/raw/images/voicevox.png?ref_type=heads)



音声の出力方法には下記の２バージョンがあります
- 音声合成ノードの起動(音声の再生は Python-sounddevice を使う)
Python-sounddevice を使い、ROSノードを起動したPCのスピーカーから音声を再生する。

- 音声合成ノードの起動(音声の再生は audio_play ノードを使う)
発話の初めに「ブツっ」というノイズが入る。



## To Do
31 Dec. 2023<br>
大幅改修中

2 Jun. 2023<br>
sound_playパッケージを使い、/audio/audioノートにデータを配信するようにしたら、発話の初めに「ブツっ」というノイズが入る。

## 動作環境
- Ubuntu 20.04
- ROS noetic
- VoiceVox Core 0.14.3
- ONNX Runtime 1.13.1


## インストール
### VoiceVoxコアのインストール
```
cd /tmp
wget https://github.com/VOICEVOX/voicevox_core/releases/download/0.14.3/voicevox_core-linux-x64-cpu-0.14.3.zip
pip3 install voicevox_core-linux-x64-cpu-0.14.3.zip
```
### ONNX Runtimeのダウンロード
リリースのページから、OS/アーキテクチャ/GPUの有無に合ったファイルをダウンロードする。

ダウンロードして解凍する場所は ~/voicevoxとする

```
mkdir ~/voicevox
cd ~/voicevox
sudo wget https://github.com/microsoft/onnxruntime/releases/download/v1.13.1/onnxruntime-linux-x64-1.13.1.tgz
sudo tar -xzvf onnxruntime-linux-x64-1.13.1.tgz
```
### 辞書ファイルのダウンロード
文章をアクセント情報付きの読みに変換するための辞書ファイルをダウンロードし、展開する。
```
cd ~/voicevox
sudo wget http://sourceforge.net/projects/open-jtalk/files/Dictionary/open_jtalk_dic-1.11/open_jtalk_dic_utf_8-1.11.tar.gz
sudo tar zxvf open_jtalk_dic_utf_8-1.11.tar.gz
```

### ONNX Runtimeと辞書ファイルの配置場所
ここでは ~/voicevox に配置したが、任意の場所に置くことができる。
下記のようにプログラム内で配置場所を指定する。

voicevox.pyを参照のこと。
```
from ctypes import CDLL
ctypes.cdll.LoadLibrary('~/voicevox/onnxruntime-linux-x64-1.13.1/lib/libonnxruntime.so')
```
```
# 辞書の読み込み
from pathlib import Path
from voicevox_core import VoicevoxCore, METAS

core = VoicevoxCore(open_jtalk_dict_dir=Path("~/voicevox/open_jtalk_dic_utf_8-1.11"))
```

### 必要なパッケージのインストール
```
sudo apt-get update
sudo apt-get install -y portaudio19-dev pulseaudio alsa-utils  ffmpeg  python3-soundfile python3-pyaudio ros-noetic-audio-common
```
```
pip3 install simpleaudio sounddevice soundfile
```

### voicevox_ros パッケージの構築
```
cd ~/catkin_ws/src/
git clone　https://gitlab.com/okadalaboratory/chat/text-to-speech/voicevox_ros.git
cd ~/catkin_ws
catkin_make
source devel/setup.bash
```

## 動作確認
### 音声合成ノードの起動(音声の再生は Python-sounddevice を使う)
Python-sounddevice を使い、ROSノードを起動したPCのスピーカーから音声を再生する。
```
roslaunch voicevox_ros voicevox_sounddevice.launch
```
```
roslaunch voicevox_ros voicevox_sounddevice.launch speaker:=10 opening:="こんにちは、ボイスボックスを使った音声合成を開始します"
```


### 音声合成ノードの起動(音声の再生は audio_play ノードを使う)
sound_playノードを使い、/audio/audioノートにデータを配信し、音声を再生する。

発話の初めに「ブツっ」というノイズが入る。

```
roslaunch voicevox_ros voicevox_audioplay.launch
```


## 音声合成の仕組み
```
rostopic list
/rosout
/rosout_agg
/voicevox/request
```
下記の通り、/voicevox/requestノードに文字列を送ることで、音声が合成されます。
```
rostopic pub /voicevox/request std_msgs/String "data: '皆さんこんにちは'"  
```
スピーカーから音声が再生されます。

## サンプルクライアントプログラム
```
import rospy
import os, sys
from std_msgs.msg import String

def speaker():
    pub = rospy.Publisher('voicevox/request', String, queue_size=10)
    rospy.init_node('speaker', anonymous=True)
    r = rospy.Rate(1) # 1hz

    str = "皆さんこんにちは"
    rospy.loginfo(str)
    pub.publish(str)
    r.sleep()

if __name__ == '__main__':
    try:
        speaker()
    except rospy.ROSInterruptException: pass
```


## 話者の変更
### 起動時のパラメータで指定する
```
roslaunch voicevox_ros voicevox_sounddevice.launch speaker:=10
```

### ROSパラメータを変更することで話者を切り替える
```
$ rosparam list 
/rosdistro
/roslaunch/uris/host_flow_z13__34517
/roslaunch/uris/host_flow_z13__41811
/rosversion
/run_id
/voicevox/speaker_id
```
下記の通り、話者を6に変更できます。　
```
rosparam set /voicevox/speaker_id 6
```

## 参考
[Google Colab ではじめる VOICEVOX](https://note.com/npaka/n/n30de0c820b1a)

[PythonとVOICEVOXで音声合成](https://zenn.dev/karaage0703/articles/0187d1d1f4d139)
- VOCIVOXコアのPythonバインディングセットアップ
Linux(x86)

```
$ wget https://github.com/VOICEVOX/voicevox_core/releases/download/0.14.3/voicevox_core-0.14.3+cpu-cp38-abi3-linux_x86_64.whl
$ pip install voicevox_core-0.14.3+cpu-cp38-abi3-linux_x86_64.whl
```

Mac(Apple Silicon)
```
$ wget https://github.com/VOICEVOX/voicevox_core/releases/download/0.14.2/voicevox_core-0.14.2+cpu-cp38-abi3-macosx_11_0_arm64.whl
$ pip install voicevox_core-0.14.2+cpu-cp38-abi3-macosx_11_0_arm64.whl
```

- ONNX Runtimeのダウンロード
Linux(x86)
```$ wget https://github.com/microsoft/onnxruntime/releases/download/v1.13.1/onnxruntime-linux-x64-1.13.1.tgz
$ tar xvzf onnxruntime-linux-x64-1.13.1.tgz
$ mv onnxruntime-linux-x64-1.13.1/lib/libonnxruntime.so.1.13.1 ./
```
Mac(Apple Silicon)
```$ wget https://github.com/microsoft/onnxruntime/releases/download/v1.13.1/onnxruntime-osx-arm64-1.13.1.tgz
$ tar xvzf onnxruntime-osx-arm64-1.13.1.tgz
$ cp onnxruntime-osx-arm64-1.13.1/lib/libonnxruntime.* ./
```
- Open Jtalkの辞書ファイルダウンロード
```
$ wget http://downloads.sourceforge.net/open-jtalk/open_jtalk_dic_utf_8-1.11.tar.gz
$ tar xvzf open_jtalk_dic_utf_8-1.11.tar.gz
```

- 動作確認用のサンプルソフトダウンロードと実行
```
$ wget https://raw.githubusercontent.com/VOICEVOX/voicevox_core/0.15.0-preview.0/example/python/run.py
$ python ./run.py ./open_jtalk_dic_utf_8-1.11 これはテストです ./audio.wav 
```

```
$ pip3 install playsound
```

```
from pathlib import Path
import voicevox_core
from voicevox_core import AccelerationMode, AudioQuery, VoicevoxCore
from playsound import playsound

SPEAKER_ID = 2

open_jtalk_dict_dir = './open_jtalk_dic_utf_8-1.11'
text = 'これはテストです'
out = Path('output.wav')
acceleration_mode = AccelerationMode.AUTO

def main() -> None:
    core = VoicevoxCore(
        acceleration_mode=acceleration_mode, open_jtalk_dict_dir=open_jtalk_dict_dir
    )
    core.load_model(SPEAKER_ID)
    audio_query = core.audio_query(text, SPEAKER_ID)
    wav = core.synthesis(audio_query, SPEAKER_ID)
    out.write_bytes(wav)
    playsound(out)


if __name__ == "__main__":
    main()
```

```
python run.py --text "おはようございます" --speaker_id 2 --root_dir_path="../../release"
```
```
# 引数の紹介
# --text 読み上げるテキスト
# --speaker_id 話者ID
# --root_dir_path [metas.json]ファイルが置いてあるパスを指定
# --f0_speaker_id 音高の話者ID（デフォルト値はspeaker_id）
# --f0_correct 音高の補正値（デフォルト値は0。+-0.3くらいで結果が大きく変わります）
```

- Q. 読み方が想定と異なります。<br>
単語の読み方は辞書登録で変更できます。設定 → 読み方＆アクセント辞書から単語を登録してみてください。

- Q. ubuntu 22.04 で動きません。<br>
libfuse2をインストールすることで解決するかもしれません。
```
sudo add-apt-repository universe
sudo apt install libfuse2
```
- Q. 電話音声など、音声のみを使いたい場合のクレジット記載はどうすれば良いですか？<br>
音声の最初や最後に音声クレジットを挿入してください。
キャラクターの利用規約に案内がある場合はそちらを優先してください。

- Q. 動画で音声を用いた場合のクレジット表記はどうすれば良いですか？<br>
動画の概要欄や動画内などに記載してください。
概要欄に記載いただければ検索などで見つけやすくなるため、VOICEVOX 開発の励みになります。

[【非公式】よくわかるVOICEVOX関連規約【個人向け】](https://note.com/dokuro_roudoku/n/n7109cdce7f5a)

### クレジット表記

#### VOICEVOX:ずんだもん
#### VOICEVOX:四国めたん
#### VOICEVOX:九州そら
#### VOICEVOX:中国うさぎ
#### VOICEVOX:春日部つむぎ


## 付録
[話者一覧と話者ID](https://ponkichi.blog/raspberry-voicevox/#st-toc-h-7)
| 話者  |  話者ID  |
| ---- | ---- |
|  四国めたん：ノーマル  |  2  |
|  四国めたん：あまあま  |  0 |
|  四国めたん：ツンツン  |  6 |
|  四国めたん：セクシー  |  4 |
| ---- | ---- |
|  ずんだもん：ノーマル  |  3  |
|  ずんだもん：あまあま  |  1 |
|  ずんだもん：ツンツン  |  7 |
|  ずんだもん：セクシー  |  5 |
| ---- | ---- |
|  春日部つむぎ：ノーマル  |  8  |
| ---- | ---- |
|  雨晴はう：ノーマル  |  10  |
| ---- | ---- |
|  波音リツ：ノーマル  |  9  |
| ---- | ---- |
|  玄野武宏：ノーマル  |  11  |
| ---- | ---- |
|  白上虎太郎：ノーマル  |  12  |
| ---- | ---- |
|  青山龍星：ノーマル  |  13  |
| ---- | ---- |
|  冥鳴ひまり：ノーマル  |  14  |
| ---- | ---- |
|  九州そら：ノーマル  |  16  |
|  九州そら：あまあま  |  15 |
|  九州そら：ツンツン  |  18 |
|  九州そら：セクシー  |  17 |
|  九州そら：ささやき  |  19 |
| ---- | ---- |
|  もち子さん：ノーマル  |  20 |
|  剣崎雌雄：ノーマル  |  21 |
|  WhiteCUL：ノーマル  |  23 |
|  WhiteCUL：たのしい  |  24 |
|  WhiteCUL：かなしい  |  25 |
|  WhiteCUL：びえーん  |  26 |
|  後鬼	人間ver.  |  27 |
|  ぬいぐるみver.  |  28 |
|  No.7:ノーマル  |  29 |
|  アナウンス  |  30 |
|  読み聞かせ  |  31 |
|  ちび式じい:ノーマル  |  42 |
|  櫻歌ミコ：ノーマル  |  43 |
|  第二形態  |  44 |
|  ロリ  |  45 |
|  小夜/SAYO:ノーマル  |  45 |
|  ナースロボ＿タイプＴ:ノーマル  |  47 |
|  楽々  |  48 |
|  恐怖  |  49 |
|  内緒話 |  50 |


